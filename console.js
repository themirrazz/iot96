//!wrt $BSPEC:{"frn":"IoT96 Console"}
const sw=new w96.StandardWindow({
    title: 'IoT96 Console',
    initialHeight: 500,
    initialWidth: 500,
    body:`<p>Make a desktop shortcut:<br>
    Name: <input class='iot-ds-name' /> <button class='iot-ds-ok'>make</button>
    </p>`,
    taskbar: true,
    center: true
});
sw.show();
sw.wndObject.querySelector('.iot-ds-ok').onclick=function() {
    w96.FS.writestr(
        'C:/user/desktop/'+sw.wndObject.querySelector('.iot-ds-name').value+".link",
        JSON.stringify({
            icon:'mime/executable',
            action:"iot96 "+JSON.stringify(sw.wndObject.querySelector('.iot-ds-name').value)
        })
    )
}
