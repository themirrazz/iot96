//!wrt $BSPEC:{"frn":"IoT96 installer"}
await w96.FS.writestr(
    "C:/system/local/bin/iot96console",`//!wrt \$BSPEC:{"frn":"IoT96 Console"}
const sw=new w96.StandardWindow({
    title: 'IoT96 Console',
    initialHeight: 500,
    initialWidth: 500,
    body:\`<p>Make a desktop shortcut:<br>
    Name: <input class='iot-ds-name' /> <button class='iot-ds-ok'>make</button>
    </p>\`,
    taskbar: true,
    center: true
});
sw.show();
sw.wndObject.querySelector('.iot-ds-ok').onclick=function() {
    w96.FS.writestr(
        'C:/user/desktop/'+sw.wndObject.querySelector('.iot-ds-name').value+".link",
        JSON.stringify({
            icon:'mime/executable',
            action:"iot96 "+JSON.stringify(sw.wndObject.querySelector('.iot-ds-name').value)
        })
    )
}
`
);
await w96.FS.writestr(
    "C:/system/local/bin/iot96",
    `//!wrt \$BSPEC:{"aut":"themirrazz","frn":"IoT96 Client","cpr":"(C) Mirrazz Inc. 2022. GPL3.0"}

try {
    if(current.boxedEnv.args[1]) {
        w96.sys.iot.sendEvent(
            'cli',
            {
                installation_name: w96.sys.iot.name,
                arg: current.boxedEnv.args[1]
            }
        )
    }
} catch(error) {
    null // no error handling or breaking
}
if(w96.sys.iot) {return}

w96.sys.iot={}

function pushEvent(type,data,user,token) {
    var xhr=new XMLHttpRequest();
    xhr.open('POST','https://api.iot96.repl.co/push-event');
    xhr.send(JSON.stringify({
        username:user,
        token:token,
        eventid:type,
        event:data
    }));
}

setTimeout(async function() {
    var session=await initApp();
    w96.sys.iot.sendEvent=function(type,event) {
        pushEvent(
            type,event,session.user,session.token
        );
    }
    w96.sys.iot.name=session.name;
    var bsod=w96.sys.renderBSOD
    w96.sys.renderBSOD=function(message) {
        w96.sys.iot.sendEvent('bsod',{
            installation_name: session.name,
            message:message
        });
        bsod(message)
    }
    if(navigator.getBattery) {
        var battery=await navigator.getBattery();
        battery.onchargingchange=()=>{
            w96.sys.iot.sendEvent(
                'battery_charging_change',
                {
                    is_charging: battery.charging?'charging':'not charing',
                    installation_name:session.name,
                    created_at: Date.now()
                }
            )
        }
    }
},3000);

function initApp() {
    return new Promise(async (y,n)=>{
        if(!await w96.FS.exists("C:/user/appdata/IoT96")) {
            await w96.FS.mkdir("C:/user/appdata/IoT96")
        }
        try {
            if(!await w96.FS.exists("C:/user/appdata/IoT96/session")) {
                throw new Error('uh oh!')
            }
            y(JSON.parse(
                await w96.FS.readstr("C:/user/appdata/IoT96/session")
            ))
        } catch (error) {
            var sw=new w96.StandardWindow({
                title: "Please, sign in, I beg you",
                taskbar: true,
                center: true,
                initialHeight: 500,
                initialWidth: 500,
                body:\`Username <input class="iot-user" /><br>
                Password <input class='iot-pass' type='password' /><br>
                Installation Name <input class='iot-name' /><br>
                <button class='iot-button'>ok</button>
                <br>
                <span style='color:red' class='iot-nope'></span>\`
            });
            sw.show();
            var user=sw.wndObject.querySelector('.iot-user');
            var pasw=sw.wndObject.querySelector('.iot-pass');
            var name=sw.wndObject.querySelector('.iot-name');
            var nope=sw.wndObject.querySelector('.iot-nope');
            sw.wndObject.querySelector('.iot-button').onclick=async()=>{
                var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=async()=>{
                    if(xhr.readyState==4) {
                        try {
                            if(JSON.parse(xhr.responseText).error) {
                                throw JSON.parse(xhr.responseText).error
                            }
                            await w96.FS.writestr('C:/user/appdata/IoT96/session',JSON.stringify({
                                token: JSON.parse(xhr.responseText).token,
                                user:user.value,
                                name:name.value
                            }));
                            sw.onclose=()=>{};
                            sw.close();
                            y({
                                token:JSON.parse(xhr.responseText).token,
                                user:user.value,
                                name:name.value
                            });
                        } catch (err) {
                            nope.innerText="Nope! "+err
                        }
                    }
                }
                xhr.onerror=()=>{
                    nope.innerText="Nope!"
                };
                xhr.open('POST','https://api.iot96.repl.co/login');
                xhr.send(JSON.stringify({
                    username:user.value,
                    password:pasw.value
                }));
            }
            sw.onclose=()=>{n()}
        }
    });
}
`
);
await w96.FS.writestr(
    'C:/system/boot/iot96.js','w96.sys.execCmd("iot96")'
);
await w96.FS.writestr(
    'C:/system/startup/IoT96.link',JSON.stringify({
        icon:'mime/executable',
        action:"iot96"
    })
);
await w96.sys.execCmd('iot96')
